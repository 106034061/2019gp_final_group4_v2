using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Computer : MonoBehaviour {
  public GameObject cam;

  void Start() {
    cam = GameObject.Find("/PlayerController/first_person_controller/world_camera");

  }

  void Update() {

  }

  void FixedUpdate() {

  }

  void OnTriggerEnter(Collider other) {
    if (other.gameObject.tag == "Player") {
      cam.GetComponent<CameraShake>().shakeDuration = 0.5f;
    }
  }
}
