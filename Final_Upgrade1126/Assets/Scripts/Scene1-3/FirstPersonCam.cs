﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonCam : MonoBehaviour {
  public float speedH = 2.0f;
  public float speedV = 2.0f;

  private float yaw;
  private float pitch;

  void Start() {
    yaw = transform.eulerAngles.x;
    pitch = transform.eulerAngles.y;
  }

  void Update() {
    yaw += speedH * Input.GetAxis("Mouse X");
    pitch -= speedV * Input.GetAxis("Mouse Y");
    transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
  }
}
