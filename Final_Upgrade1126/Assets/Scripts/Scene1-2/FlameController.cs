﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameController : MonoBehaviour
{
    public int Flame_Mode = 1;
    public GameObject Pivot;
    public GameObject Player;
    private float Current_Distance;
    private float Previous_Distance;
    private float Delta_Distance;
    public GameObject[] lights = new GameObject[Light_Number];
    static public int Light_Number = 1;
    public int Light_Transfer_Index = 0;
    public float Light_Intensity = 10000;
    void Start()
    {
        InvokeRepeating("FlameUpdate", 0.5f, 3.0f);
        // lights[0] = GameObject.Find("TunnelController/FlameController/Spot Light");

    }
    void FlameUpdate(){
        Current_Distance = Vector3.Distance(Player.transform.position, Pivot.transform.position);
        Delta_Distance = Current_Distance - Previous_Distance;
        if(Flame_Mode == 1){
            if(Delta_Distance > 0){
                for(int i=0; i < Light_Number ;i++)
                    lights[i].GetComponent<Light>().intensity -= Light_Intensity;
            }
            else if( Delta_Distance < 0){
                for(int i=0; i < Light_Number;i++)
                    lights[i].GetComponent<Light>().intensity += Light_Intensity;
            }
        }
        else if(Flame_Mode == 0){
            if(Delta_Distance > 0){
                for(int i=0; i < Light_Number;i++)
                    lights[i].GetComponent<Light>().intensity += Light_Intensity;
            }
            else if( Delta_Distance < 0){
                for(int i=0; i < Light_Number;i++)
                    lights[i].GetComponent<Light>().intensity -= Light_Intensity;
            }
        }
        Previous_Distance = Current_Distance;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
