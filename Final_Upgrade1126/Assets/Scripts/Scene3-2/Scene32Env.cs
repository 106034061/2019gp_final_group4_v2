using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scene32Env : MonoBehaviour {
  public string prevScore;

  public GameObject page1;

  public GameObject page2;

  public GameObject page3;
  public GameObject[] page3_scores;
  private GameObject page3_score;
  public GameObject page3_btn_easy;
  public GameObject page3_btn_hard;

  public GameObject page_easy;
  public GameObject[] page4_easy_scores;
  public GameObject page_hard;
  public GameObject[] page4_hard_scores;
  private GameObject page4;
  private GameObject page4_score;

  float countdown_before_load_next;

  void Start() {
    GameObject[] gs = GameObject.FindGameObjectsWithTag("scene3score");
    prevScore = gs.Length > 0 ? gs[0].name : "0";
    page1.SetActive(true);
    countdown_before_load_next = 3f;
  }

  void FixedUpdate() {
    if (countdown_before_load_next > 0f) {
      countdown_before_load_next -= Time.deltaTime;
      if (countdown_before_load_next <= 0f) {
        if (page1.activeSelf) {
          page1.SetActive(false);
          page2.SetActive(true);
          countdown_before_load_next = 3f;
        } else if (page2.activeSelf) {
          page2.SetActive(false);
          page3.SetActive(true);
          page3_score = page3_scores[0];
          for (int i = 0; i < page3_scores.Length; ++i) {
            if (page3_scores[i].name == prevScore) page3_score = page3_scores[i];
          }
          page3_score.SetActive(true);
          page3_btn_easy.GetComponent<Button>().onClick.AddListener(to_easy);
          page3_btn_hard.GetComponent<Button>().onClick.AddListener(to_hard);
        }
      }
    }
  }

  void to_easy() {
    page4 = page_easy;
    page4.SetActive(true);
    page4_score = page4_easy_scores[0];
    for (int i = 0; i < page4_easy_scores.Length; ++i) {
      if (page4_easy_scores[i].name == prevScore) page4_score = page4_easy_scores[i];
    }
    page4_score.SetActive(true);
    page3.SetActive(false);
  }

  void to_hard() {
    page4 = page_hard;
    page4.SetActive(true);
    page4_score = page4_hard_scores[0];
    for (int i = 0; i < page4_hard_scores.Length; ++i) {
      if (page4_hard_scores[i].name == prevScore) page4_score = page4_hard_scores[i];
    }
    page4_score.SetActive(true);
    page3.SetActive(false);
  }
}
