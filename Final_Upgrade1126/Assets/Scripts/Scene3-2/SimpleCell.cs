using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SimpleCell : MonoBehaviour, IPointerClickHandler {
  public Image mOutlineImage;

  public void OnPointerClick(PointerEventData eventData) {
    if (eventData.button == PointerEventData.InputButton.Left) {
      EasyMSController.leftClickCell(gameObject);
    } else if (eventData.button == PointerEventData.InputButton.Middle) {
      Debug.Log("Middle click");
    } else if (eventData.button == PointerEventData.InputButton.Right) {
      EasyMSController.rightClickCell(gameObject);
    }
  }
}

