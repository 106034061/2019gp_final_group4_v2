﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scene31Env : MonoBehaviour {
  public GameObject page1;
  public GameObject page1_bg;
  public GameObject page1_startbtn;

  public GameObject page2;
  public GameObject page2_btn_easy;
  public GameObject page2_btn_hard;

  public GameObject page_easy;
  public GameObject page3_easy_Q1;
  public GameObject page3_easy_A1;
  public GameObject page3_easy_A2;
  public GameObject page3_easy_Q2;
  public GameObject page3_easy_B1;
  public GameObject page3_easy_B2;
  public GameObject page3_easy_dest1;
  public GameObject page3_easy_dest2;
  public GameObject page3_easy_score1;
  public GameObject page3_easy_score2;

  public GameObject page_hard;
  public GameObject page3_hard_Q1;
  public GameObject page3_hard_A1;
  public GameObject page3_hard_A2;
  public GameObject page3_hard_Q2;
  public GameObject page3_hard_B1;
  public GameObject page3_hard_B2;
  public GameObject page3_hard_dest1;
  public GameObject page3_hard_dest2;
  public GameObject page3_hard_score1;
  public GameObject page3_hard_score2;

  private GameObject page3;
  private GameObject page3_Q1;
  private GameObject page3_A1;
  private GameObject page3_A2;
  private GameObject page3_Q2;
  private GameObject page3_B1;
  private GameObject page3_B2;
  public GameObject page3_dest;
  private GameObject page3_score1;
  private GameObject page3_score2;
  public int page3_solved = 0;

  private float delay;
  public float countdown_before_load_next;

  void Start() {
    delay = 0f;
    page1.SetActive(true);
    page1_bg.SetActive(true);
    page1_startbtn.SetActive(true);
    page1_startbtn.GetComponent<Button>().onClick.AddListener(start_game);
  }

  void FixedUpdate() {
    if (page3_solved == 1) {
      if (delay == 0f) delay = 2f;
      else if (delay > 0f) {
        delay -= Time.deltaTime;
        if (delay <= 0f) {
          delay = -1f;
          page3_Q1.SetActive(false);
          page3_A1.SetActive(false);
          page3_A2.SetActive(false);
          page3_score1.SetActive(true);
          page3_Q2.SetActive(true);
          page3_B1.SetActive(true);
          page3_B2.SetActive(true);
          page3_dest = page3_dest == page3_easy_dest1 ? page3_easy_dest2 : page3_hard_dest2;
        }
      }
    } else if (page3_solved == 3) {
      page3_score2.SetActive(true);
      countdown_before_load_next -= Time.deltaTime;
      if (countdown_before_load_next <= 0f) SceneManager.LoadScene("Scene3-2(minesweeper)");
    }
  }

  void start_game() {
    page1.SetActive(false);
    page1_bg.SetActive(false);
    page1_startbtn.SetActive(false);
    page2.SetActive(true);
    page2_btn_easy.GetComponent<Button>().onClick.AddListener(to_easy);
    page2_btn_hard.GetComponent<Button>().onClick.AddListener(to_hard);
  }

  void to_easy() {
    page3 = page_easy;
    page3_Q1 = page3_easy_Q1;
    page3_A1 = page3_easy_A1;
    page3_A2 = page3_easy_A2;
    page3_Q2 = page3_easy_Q2;
    page3_B1 = page3_easy_B1;
    page3_B2 = page3_easy_B2;
    page3_dest = page3_easy_dest1;
    page3_score1 = page3_easy_score1;
    page3_score2 = page3_easy_score2;
    page3.SetActive(true);
    page3_Q1.SetActive(true);
    page3_A1.SetActive(true);
    page3_A2.SetActive(true);
    page2.SetActive(false);
  }

  void to_hard() {
    page3 = page_hard;
    page3_Q1 = page3_hard_Q1;
    page3_A1 = page3_hard_A1;
    page3_A2 = page3_hard_A2;
    page3_Q2 = page3_hard_Q2;
    page3_B1 = page3_hard_B1;
    page3_B2 = page3_hard_B2;
    page3_dest = page3_hard_dest1;
    page3_score1 = page3_hard_score1;
    page3_score2 = page3_hard_score2;
    page3.SetActive(true);
    page3_Q1.SetActive(true);
    page3_A1.SetActive(true);
    page3_A2.SetActive(true);
    page2.SetActive(false);
  }
}
