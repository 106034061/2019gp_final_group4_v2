﻿using UnityEngine;
using UnityEngine.UI;

namespace Footsteps {

	public class DemoUI : MonoBehaviour {

		[SerializeField] GameObject topDownController;
		[SerializeField] GameObject topDownCamera;
		[SerializeField] GameObject firstPersonController;
		private bool FirstPersonView = true;
		void Update() {
			if(Input.GetKey(KeyCode.C)){
				if(FirstPersonView){
					ActivateTopDown();
					FirstPersonView = false;
				}
				else{
					ActivateFirstPerson();
					FirstPersonView = true;

				}
			}
		}

		public void ActivateTopDown() {
			if(!topDownController.activeSelf) topDownController.transform.position = firstPersonController.transform.position;
			firstPersonController.SetActive(false);
			topDownController.SetActive(true);
			topDownCamera.SetActive(true);
		}

		public void ActivateFirstPerson() {
			if(!firstPersonController.activeSelf) firstPersonController.transform.position = topDownController.transform.position;

			firstPersonController.SetActive(true);
			topDownController.SetActive(false);
			topDownCamera.SetActive(false);
		}
	}
}
